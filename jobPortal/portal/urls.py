from django.conf.urls import patterns, include, url
from django.views.generic import ListView
from portal.models import Advertisement

urlpatterns = patterns('portal.views',
    url(r'^$', 'home'),
    url(r'^about/$', 'about'),
    url(r'^add-advertisement/$', 'advertisement_register'),
    url(r'^add-resume/$', 'add_resume'),
    url(r'^account/change-password/$', 'change_password'),
    url(r'^special_ad_register/(?P<id>\d+)$', 'special_ad_register'),
    url(r'^account/$', 'page_acount_jobSeeker'),
    url(r'^faq/', 'faq'),
    url(r'^register/seeker/', 'register_seeker'),
    url(r'^register/employer/', 'register_employer'),
    url(r'^ads/$', 'advertisements'),
    url(r'^adss/$', 'advertisement'),
    url(r'^adss/request/', 'ad_request'),
    url(r'^resume/$', 'resume'),
    url(r'^resumes/$', 'resumes'),
    url(r'^resume/request/$', 'request_resume'),
    url(r'^festivals/$', 'festivals'),
    url(r'^festivals/register/$', 'register_festival'),
    url(r'^festivals/register/payment', 'festival_registered'),
    url(r'^ajax/account/history', 'account_history'),
    url(r'^search/$', 'search'),
    url(r'^account/evaluation/$', 'evaluation'),
    url(r'^updateSeeker/$', 'update_seeker'),
    url(r'^updateEmployer/$', 'update_employer'),
    url(r'^userResume/$', 'userResume'),
    url(r'^userAd/$', 'userAd'),
    url(r'^jobrequests/$', 'jobrequests'),
    url(r'^empJobrequests/$', 'employer_jobrequests'),
    url(r'^empJobproposal/$', 'employer_jobproposal'),
    url(r'^empAutomaticMatch/$', 'employer_automaticMatch'),
    url(r'^empSuccessfuls/$', 'employer_successfuls'),
    url(r'^jobproposal/$', 'jobproposal'),
    url(r'^automaticMatch/$', 'automaticMatch'),
    url(r'^successfuls/$', 'successfuls'),
    url(r'^forgotpass/$', 'forgot_password'),
    url(r'^resetpass', 'reset_password'),
    url(r'^account/contact_us/$', 'contact_us'),
    url(r'^updateResume/(?P<id>\d+)', 'update_resume'),
    url(r'^updateAd/(?P<id>\d+)', 'update_ad'),
    url(r'^resume/(?P<email>.+)', 'anotherUser_res'),
    url(r'^removeAd/(?P<id>\d+)', 'remove_ad'),
    url(r'^removeResume/(?P<id>\d+)', 'remove_res'),
    url(r'^accreq/(?P<id>\d+)', 'accept_request'),
    url(r'^rejreq/(?P<id>\d+)', 'reject_request'),
    url(r'^empaccreq/(?P<id>\d+)', 'emp_accept_request'),
    url(r'^emprejreq/(?P<id>\d+)', 'emp_reject_request'),

    
    

    
    
    

    
    
    

)

urlpatterns += patterns('',
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),
)
urlpatterns += patterns('',
    url(r'^ads/bysalary$', ListView.as_view(
    queryset=Advertisement.objects.all().order_by('-salary'),
    context_object_name='ads',
    template_name='advertisements.html', 
    )),
    url(r'^ads/bydate$', ListView.as_view(
    queryset=Advertisement.objects.all().order_by('-date'),
    context_object_name='ads',
    template_name='advertisements.html', 
    )),
)
