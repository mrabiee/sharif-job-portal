from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User as djangoUser
from os import urandom
import datetime
from django.utils import timezone
from base64 import urlsafe_b64encode
from os.path import splitext


GENDER_CHOICES = (
    ('F', _("female")),
    ('M', _("male")),
)
STATE_CHOICES = (
    ('A', _('Active')),
    ('D', _('Deactive')),
)
CATEGORY_STATE = (
    ('SE', _("Software engineer")),
    ('ME', _("Mechanical engineer"))
)
CITY_CHOICES = (
    ('T', _('Tehran')),
    ('E', _('Esfahan')),
    ('M', _('Mashhad')),
    ('S', _('Semnan')),
    ('A', _('Arak')),
)

def sluggenerator():
    return random_generator(20) 

random_generator = lambda random_size: urlsafe_b64encode(urandom(random_size))
def get_random_filename_generator(fn_format, default_extension='.jpg', random_size=12):
    """Returns a random filename generator suitable for upload_to of django FileField"""

    def random_filename(instance, filename):
        split = splitext(filename)
        extension = split[-1] if len(split) > 1 else default_extension
        return fn_format % (random_generator(random_size), extension)

    return random_filename
    
def sluggenerator():
    return random_generator(20) 
    
class User(models.Model):
    user = models.ForeignKey(djangoUser)
    name = models.CharField(verbose_name=_("Full name"), max_length=250,
            help_text=_("This name should be identical to the name of the owner of the back account for future payments"))
    telephone = models.CharField(verbose_name=_("Telephone number"), max_length=15, blank=True)
    mobilephone = models.CharField(verbose_name=_("Mobile phone number"), max_length=15, blank=True)
    address = models.TextField(verbose_name=_("Contact address"))
    postal_code = models.CharField(verbose_name=_("Postal code"),
            max_length=10, blank=True)
    email = models.EmailField(verbose_name=_("Email"))
    payment = models.OneToOneField("Payment", verbose_name=_("Payment"), null=True, blank=True)
    register_date = models.DateField(_("Registration date"), default=datetime.date.today)

    class Meta:
        verbose_name = _('account')
        verbose_name_plural = _('accounts')
    def __unicode__(self):
        return "%s" %self.name




class JobSeeker(User):
    national_code = models.CharField(verbose_name=_("National code"),
            max_length=10)
    date_of_birth = models.DateField(verbose_name=_("Date of birth"))
    gender = models.CharField(verbose_name=_("Gender"), max_length=1,
                                         default='M', choices=GENDER_CHOICES)
    class Meta:
        verbose_name = _('job seeker')
        verbose_name_plural = _('job seekers')


class Employer(User):
    economic_code = models.CharField(verbose_name=_("Economic code"),
            max_length=12)
    registration_number = models.CharField(verbose_name=_("Registration number"),
            max_length=10)
    VERIFICATION_STATE_CHOICES = (
        ('N', _("not reviewed")),

        ('A', _("verified")),
        ('M', _("modified - was verified")),

        ('D', _("defective")),
        ('F', _("modified - was defective")),
    )
    verified_by_human = models.CharField(verbose_name=_("Reviewed by Human"), max_length=1,
                                         default='N', choices=VERIFICATION_STATE_CHOICES)

    class Meta:
        verbose_name = _('employer')
        verbose_name_plural = _('employers')
   


class Resume(models.Model):
    job_experiences = models.TextField(verbose_name=_("Job experiences"))
    EDUCATION_CHOICES = (
        ('DI', _('Diploma')),
        ('BS', _('Bachelor of Science')),
        ('MS', _('Master of Science')),
        ('PH', _('PHD')),
        ('PD', _('Post Doc')),
    )
    city = models.CharField(_("City"), max_length=1, default='T', choices=CITY_CHOICES)
    education = models.CharField(verbose_name=_("Education"),max_length=2, default='DI', choices=EDUCATION_CHOICES)
    skills = models.TextField(verbose_name=_("Skills"))
    state = models.CharField(_("state"), max_length=1, default='A', choices=STATE_CHOICES)
    job_seeker = models.ForeignKey(JobSeeker, verbose_name=_("Job seeker"), db_index=True)
    category = models.CharField(_("Category"), max_length=2, default='S', choices=CATEGORY_STATE)
    additional_informatiom = models.TextField(null=True,blank =True)
    profile_pic=models.FileField(_("Profile picture"), upload_to=get_random_filename_generator("photos/%s%s"), null = True ,blank=True)

    class Meta:
        verbose_name = _('resume')
        verbose_name_plural = _('resumes')
    def __unicode__(self):
        return "%s" %self.job_seeker.name



class Advertisement(models.Model):
    city = models.CharField(_("City"), max_length=1, default='T', choices=CITY_CHOICES)
    salary = models.PositiveIntegerField(_("Salary"), default=0, help_text=_("Tomans"))
    GENDER_CHOICES_BOTH = (
        ('F', _('female')),
        ('M', _('male')),
        ('B', _('both')),
    )
    gender = models.CharField(verbose_name=_("Gender"), max_length=1,
                                         default='B', choices=GENDER_CHOICES_BOTH)
    age = models.IntegerField(_("Age"), default=18)
    number_of_people = models.IntegerField(_("Number of required people"), default=1)
    state = models.CharField(_("state"), max_length=1, default='A', choices=STATE_CHOICES)
    required_skills = models.TextField(_("Required skills"))
    work_hours = models.IntegerField(_("Work hours"), default=8)
    addres = models.TextField(_("Address"))
    category = models.CharField(_("Category"), max_length=2, default='S', choices=CATEGORY_STATE)
    employer = models.ForeignKey(Employer, null = True)
    date = models.DateTimeField(_("Date"),default=timezone.now)
    logo = models.FileField(_("Logo"), upload_to=get_random_filename_generator("photos/%s%s"), null = True )

    EDUCATION_CHOICES = (
        ('DI', _('Diploma')),
        ('BS', _('Bachelor of Science')),
        ('MS', _('Master of Science')),
        ('PH', _('PHD')),
        ('PD', _('Post Doc')),
    )
    education = models.CharField(verbose_name=_("Education"),max_length=2, default='DI', choices=EDUCATION_CHOICES)

    class Meta:
        verbose_name = _('advertisement')
        verbose_name_plural = _('advertisements')
    def __unicode__(self):
        return "%s" %self.employer.name


class SpecialAdvertisement(models.Model):
    advertisement = models.ForeignKey(Advertisement, verbose_name=_("Advertisement"))
    from_date = models.DateField(_("From date"), default=datetime.date.today)
    to_date = models.DateField(_("To date"), default=datetime.date.today)
    payment = models.OneToOneField("Payment", verbose_name=_("Payment"), null=True)

    class Meta:
        verbose_name = _('special advertisement')
        verbose_name_plural = _('special advertisements')



class Festival(models.Model):
    from_date = models.DateTimeField(_("From date"))
    to_date = models.DateTimeField(_("To date"))
    title = models.CharField(_("Title"), max_length=50)
    description = models.TextField(_("Description"))
    poster = models.FileField(upload_to=get_random_filename_generator("photos/%s%s"), null = True )
    discount = models.PositiveIntegerField(_("Discount"),default=0)
    price = models.PositiveIntegerField(_("Price"),default=0, help_text=_("Tomans"))


    class Meta:
        verbose_name = _('festival')
        verbose_name_plural = _('festivals')
    def __unicode__(self):
        return "%s" %self.title



class Payment(models.Model):
    amount = models.PositiveIntegerField(_("Amount"), default=0, help_text=_("Tomans"))
    refrence_code = models.CharField(_("Refrence code"), max_length=30,default= sluggenerator)

    class Meta:
        verbose_name = _('payment')
        verbose_name_plural = _('payments')
    def __unicode__(self):
        return "%s" %self.refrence_code


class RegisterFestival(models.Model):
    employer = models.ForeignKey(Employer)
    festival = models.ForeignKey(Festival)
    payment = models.ForeignKey(Payment)
    

    class Meta:
        verbose_name = _('register festival')
        verbose_name_plural = _('register festivals')
    def __unicode__(self):
        return "%s" %self.festival.title + "_" + self.employer.name


class JobRequest(models.Model):
    # resume = models.ForeignKey(Resume)
    advertisement = models.ForeignKey(Advertisement)
    job_seeker = models.ForeignKey(JobSeeker)
    SUCCESS_CHOICES = (
        ('F', _('False')),
        ('T', _('True')),
        ('P', _('Pending'))
    )

    success = models.CharField(_("Success"), default='P', blank=True, choices =SUCCESS_CHOICES, max_length = 1)
    DIRECTION_CHOICES = (
        ('S', _('Job seeker')),
        ('E', _('Employer')),
        ('Y', _('System')),
    )
    direction = models.CharField(_("Direction"), choices=DIRECTION_CHOICES, max_length=1)
    hide_seeker = models.BooleanField(_("Hide_job_seeker"), default = False)
    hide_employer = models.BooleanField(_("Hide_employer"),default = False)

    class Meta:
        verbose_name = _('job request')
        verbose_name_plural = _('job requests')
    def __unicode__(self):
        return "%s" %self.advertisement.employer.name+"_"+self.job_seeker.name


class Comment(models.Model):
    
    text = models.TextField(_("Text"))
    KIND_CHOICES = (
        ('S', _('Suggest')),
        ('C', _('Critic')),
    )
    kind = models.CharField(_("Kind"), choices=KIND_CHOICES, max_length=1)
    user = models.ForeignKey(User)
    register_date = models.DateField(_("Registration date"), default=datetime.date.today)

    class Meta:
        verbose_name = _('comment')
        verbose_name_plural = _('comments')
    def ___unicode___(self):
        return "%s" %self.kind


class AdComment(models.Model):
    
    text = models.TextField(_("Text"))
    advertisement = models.ForeignKey(Advertisement, null = True)
    register_date = models.DateField(_("Registration date"), default=datetime.date.today)

    class Meta:
        verbose_name = _('adcomment')
        verbose_name_plural = _('adcomments')
    def ___unicode___(self):
        return "%s" %self.register_date
    
    
class FAQ(models.Model):
    question = models.TextField(_("Question"), default = '?')
    answer = models.TextField(_("Answer"), default = ' ')
    class Meta:
        verbose_name = _('FAQ')
        verbose_name_plural = _('FAQs')
    def ___unicode___(self):
        return "%s" %self.question
