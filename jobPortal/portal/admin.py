from django.contrib import admin

from portal.models import *

class ResumeAdmin(admin.ModelAdmin):
	list_display = ('job_seeker', 'category')
	list_filter = ('state','education','category')

class CommentAdmin(admin.ModelAdmin):
	list_display = ('text', 'user')
	
class PaymentAdmin(admin.ModelAdmin):
	list_display = ('refrence_code', 'amount')
	
class JobRequestAdmin(admin.ModelAdmin):
	list_display = ('advertisement','job_seeker','direction')
	
class FAQAdmin(admin.ModelAdmin):
	list_display = ('question',)
	
class AdvertisementAdmin(admin.ModelAdmin):
	list_display = ('employer', 'category','education')
	list_filter = ('state','category','city')
	
class UserAdmin(admin.ModelAdmin):
	list_display = ('name', 'email')
	
class SpecialAdAdmin(admin.ModelAdmin):
	list_display = ('advertisement',)
	
	 

admin.site.register(User,UserAdmin)
admin.site.register(JobSeeker)
admin.site.register(Employer)
admin.site.register(Resume,ResumeAdmin)
admin.site.register(Advertisement,AdvertisementAdmin)
admin.site.register(SpecialAdvertisement,SpecialAdAdmin)
admin.site.register(Festival)
admin.site.register(Payment,PaymentAdmin)
admin.site.register(RegisterFestival)
admin.site.register(JobRequest,JobRequestAdmin)
admin.site.register(Comment,CommentAdmin)
admin.site.register(FAQ,FAQAdmin)

	

