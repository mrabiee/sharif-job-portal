#coding=utf-8
# -*- coding: utf-8 -*-

import datetime
from django.shortcuts import render
from portal.models import *
from portal.models import JobRequest, User, JobSeeker, Employer, Advertisement, FAQ,Resume, CITY_CHOICES, CATEGORY_STATE
from portal.utils import *
from portal.forms import SeekerRegisterForm, EmployerRegisterForm, SpecialAdvertisementRegisterForm, SeekerUpdate, CommentForm, AdvertisementRegisterForm,EmployerUpdate,ResumeRegisterForm,ForgetPassForm,ChangePassForm
from django.db.models import Sum, Count

from django.contrib import messages
from django.contrib.auth.views import password_change
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from django.utils.translation import ugettext_lazy as _

import json
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
import base64
from os import urandom


from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags



def home(request): 
    #mostSeen = JobRequest.objects.filter(direction = 'S')
    mostSeen = Advertisement.objects.all()
    mostSeen = mostSeen.annotate(Count('jobrequest')).order_by('-jobrequest__count')
    #mostSeen = mostSeen.values('advertisement').annotate(Count('advertisement'))
    lastAds = Advertisement.objects.all().order_by('-id')
    special_ads = SpecialAdvertisement.objects.filter(to_date__gte=datetime.date.today())
    return render(request, "home.html",{'lastAds' : lastAds[0:10], 'category_choices': CATEGORY_STATE, 'city_choices': CITY_CHOICES, 'mostSeen' : mostSeen[:10], 'special_ads': special_ads})

def search(request):
    search_in = request.GET.get('search-in', None)
    cat = request.GET.get('category', None)
    city = request.GET.get('city', None)
    q = request.GET.get('q', None)
    if search_in == 'A':
        ads = Advertisement.objects.filter(state='A')
        if cat and cat != '':
            ads = ads.filter(category=cat)
        if city and city != '':
            ads = ads.filter(city=city)
        if q and q != '':
            query = get_query(q, ['required_skills', 'addres',])
            ads = ads.filter(query)
        ads_bysalary = ads.order_by('salary')
        ads = ads.order_by('id')
        return advertisements(request, ads, ads_bysalary)
    else:
        resumes_search = Resume.objects.filter(state='A')
        if cat and cat != '':
            resumes_search = resumes_search.filter(category=cat)
        if q and q != '':
            query = get_query(q, ['job_experiences', 'skills', 'education',])
            resumes_search = resumes_search.filter(query)
        resumes_search = resumes_search.order_by('id')
        return resumes(request, resumes_search)
        

def about(request):
    return render(request, "about.html")


@login_required
def page_acount_jobSeeker(request):
    j = JobSeeker.objects.filter(user = request.user)
    e = Employer.objects.filter(user = request.user)
    flag = True
    print(request.user)
    if(len(e) > 0):
        print("employer")
        flag = False
        j = e    
    account =j[0]  
    return render(request, "sidebar.html", {'account':account, 'flag': flag})

#def automatic_match():
#    ad = Advertisement.objects.filter(state = 'A')
#    return 
@login_required
def userResume(request):
    seeker = JobSeeker.objects.get(user = request.user)
    resumes = Resume.objects.filter(job_seeker = seeker)
    return render(request, "userResume.html", {'resumes' : resumes, 'flag': True})
@login_required    
def userAd(request):
    sads = SpecialAdvertisement.objects.filter(advertisement__employer__user=request.user, to_date__gte=datetime.date.today())
    temp = sads.values_list('advertisement__id')
    ids = []
    for t in temp:
        ids.append(t[0])
    ads = Advertisement.objects.filter(employer__user=request.user).exclude(id__in=ids)
    return render(request, "userAd.html", {'ads' : ads, 'sads':sads})

    
        
    
@csrf_exempt
def account_history(request):
    data = request.POST
    if data['flag'] == 'T':
        account1 = JobSeeker.objects.filter(user = request.user)
        account = account1[0]
        adver = JobRequest.objects.filter(job_seeker = account)
        if data['type'] == 'a':
            result = adver.filter(direction = 'S')[:9]
        if data['type'] == 'b':
            result = adver.filter(direction = 'E')[:9] 
    else:
        account1 = Employer.objects.filter(user = request.user)
        account = account1[0]
        adver = JobRequest.objects.filter(advertisement__employer = account)
        if data['type'] == 'a':
            result = adver.filter(direction = 'E')[:9]
        if data['type'] == 'b':
            result = adver.filter(direction = 'S')[:9]                  
    if data['type'] == 'c':
        if 'id' in data:
            delete = JobRequest.objects.get(pk = data['id'])
            if data['flag'] == 'T':
                delete.hide_seeker = True
                delete.save()
            else:
                delete.hide_employer = True
                delete.save()
        if data['flag'] == 'T':
            result = adver.filter(direction = 'Y', hide_seeker = False)[:9]
        else:
            result = adver.filter(direction = 'Y', hide_employer = False)[:9]
        
    final = []
    for p in result:
        if p.success == 'F':
            claz = "label label-danger"
        elif p.success == 'T':
            claz = "label label-success"
        else:
            claz = "label label-info"
        if p.advertisement.state =='D':
            color = "label label-warning"   
        else :
            color = "label label-primary"
        final.append({"job_seeker": p.job_seeker.name,"employer": p.advertisement.employer.name,"category":p.advertisement.get_category_display(), "class": claz, "status":color, "pk": p.pk, 'adid' : p.advertisement.pk, 'email' : p.job_seeker.email})
    #print(final)	
    responce_data = {}
    responce_data['result'] = final
    
        
    return HttpResponse(json.dumps(responce_data), content_type="application/json")
    
@login_required
def employer_jobrequests(request):
    account1 = Employer.objects.filter(user = request.user)
    account = account1[0]
    adver = JobRequest.objects.filter(advertisement__employer = account)
    result = adver.filter(direction = 'E')#
    return render(request, 'empJobRequests.html', {'result': result, 'flag' : False, 'mod': 'a'})

    
    
@login_required    
def jobrequests(request):
    account1 = JobSeeker.objects.filter(user = request.user)
    account = account1[0]
    adver = JobRequest.objects.filter(job_seeker = account)
    result = adver.filter(direction = 'S')
    return render(request, 'jobRequests.html', {'result': result, 'flag' : True, 'mod': 'a'})
@login_required   
def employer_jobproposal(request):
    account1 = Employer.objects.filter(user = request.user)
    account = account1[0]
    adver = JobRequest.objects.filter(advertisement__employer = account)
    result = adver.filter(direction = 'S')# 
    return render(request, 'empJobRequests.html', {'result': result, 'flag' : False, 'mod': 'b'})           
    

@login_required    
def jobproposal(request):
    account1 = JobSeeker.objects.filter(user = request.user)
    account = account1[0]
    adver = JobRequest.objects.filter(job_seeker = account)
    result = adver.filter(direction = 'E')
    return render(request, 'jobRequests.html', {'result': result, 'flag' : True, 'mod': 'b'})
@login_required     
def employer_automaticMatch(request):
    account1 = Employer.objects.filter(user = request.user)
    account = account1[0]
    adver = JobRequest.objects.filter(advertisement__employer = account)
    result = adver.filter(direction = 'Y', hide_employer = False)
    return render(request, 'empJobRequests.html', {'result': result, 'flag' : False, 'mod': 'c'})  

    
@login_required   
def automaticMatch(request):
    account1 = JobSeeker.objects.filter(user = request.user)
    account = account1[0]
    adver = JobRequest.objects.filter(job_seeker = account)
    result = adver.filter(direction = 'Y', hide_seeker = False)#
    return render(request, 'jobRequests.html', {'result': result, 'flag' : True, 'mod': 'c'})
@login_required    
def employer_successfuls(request):
    account1 = Employer.objects.filter(user = request.user)
    account = account1[0]
    adver = JobRequest.objects.filter(advertisement__employer = account)
    result = adver.filter(success = 'T').exclude(direction = 'Y')
    return render(request, 'empJobRequests.html', {'result': result, 'flag' : False, 'mod': 'd'})  

    

@login_required
def successfuls(request):
    account1 = JobSeeker.objects.filter(user = request.user)
    account = account1[0]
    adver = JobRequest.objects.filter(job_seeker = account)
    result = adver.filter(success = 'T').exclude(direction = 'Y')
    return render(request, 'jobRequests.html', {'result': result, 'flag' : True, 'mod' : 'd'})

def anotherUser_res(request, email):
    j = JobSeeker.objects.get(email = email)
    #print(j.name)
    res = Resume.objects.filter(job_seeker = j, state = 'A')
    #print(res)
    return render(request, 'userResume.html', {'resumes': res, 'flag' : False})

    
    
@login_required    
def update_seeker(request):
    seeker = JobSeeker.objects.get(user = request.user)
    if request.method == "POST":
        form = SeekerUpdate(request.user,request.POST, instance = seeker)
        print("request.post ", request.POST)
        if form.is_valid():
            seeker = form.save()
            return HttpResponseRedirect("/account/")
    
    else:
        form = SeekerUpdate(request.user,instance=seeker)
    return render(request, 'update.html', {'form': form, 'flag' : True})
@login_required    
def update_employer(request):
    employer = Employer.objects.get(user = request.user)
    if request.method == "POST":
        form = EmployerUpdate(request.user,request.POST, instance = employer)
        print("request.post ", request.POST)
        if form.is_valid():
            employer = form.save()
            return HttpResponseRedirect("/account/")
    
    else:
        form = EmployerUpdate(request.user,instance=employer)
    return render(request, 'update.html', {'form': form, 'flag' : False})
    

def register(request):
    if request.method == "POST":
        form = SeekerRegisterForm(request.POST)
        if form.is_valid():
            seeker = form.save()
            return HttpResponseRedirect("/")
    else:
        form = SeekerRegisterForm()
    return render(request, 'register.html', {'form': form,})

@employer_required
def register_festival(request):
    festival = Festival.objects.get(id=request.GET.get('id'))
    if request.user.is_authenticated():#login bashe
        employer= Employer.objects.filter(user=request.user)
        if employer:
            return render(request, 'festival_register.html', {'employer': employer[0],'festival':festival})
    messages.add_message(request, messages.INFO, "you cant register with this account , sign in with an employer account or register")
    return render(request, 'festival_register.html',)
    
def festival_registered(request):
    employer= Employer.objects.filter(user=request.user)[0]
    festival = Festival.objects.get(id=request.GET.get('id'))
    #payment = Payment(amount=100-(festival.discount * 100))
    price = festival.price*(1-(festival.discount/100))
    payment = Payment(amount=price)############
    payment.save()
    registered = RegisterFestival(employer=employer,festival=festival,payment=payment)
    registered.save()
    message='شما با موفقیت در جشنواره ثبت نام شدید.'
    return render(request, 'alert.html',{'message':message})

def register_seeker(request):
    if request.method == "POST":
        form = SeekerRegisterForm(request.POST)
        if form.is_valid():
            seeker = form.save()
            return HttpResponseRedirect("/")
    else:
        form = SeekerRegisterForm()
    return render(request, 'register.html', {'form': form,})

def register_employer(request):
    if request.method == "POST":
        form = EmployerRegisterForm(request.POST)
        if form.is_valid():
            seeker = form.save()
            return HttpResponseRedirect("/")
    else:
        form = EmployerRegisterForm()
    return render(request, 'register.html', {'form': form,})

    
def advertisements(request, ads=None, ads_bysalary=None):
    if request.GET.get('bysalary'):
        ads = Advertisement.objects.filter(state='A').order_by('salary')
    elif ads == None:
        ads = Advertisement.objects.filter(state='A').order_by('id')
    paginator = Paginator(ads, 6) # Show 6 contacts per page
    page = request.GET.get('page')
    try:
        ads = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        ads = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        ads = paginator.page(paginator.num_pages)
    return render(request, "advertisements.html",{'ads' : ads,'ads2':ads_bysalary,})

@login_required
def advertisement(request):
    id = request.GET.get('id')
    ad = Advertisement.objects.get(id=id)
    similarAds = Advertisement.objects.filter(category=ad.category)
    is_jobseeker = False
    if JobSeeker.objects.filter(user = request.user):
        is_jobseeker = True
    return render(request, "advertisement.html",{'ad' : ad, 'similarAds':similarAds[:6], 'is_jobseeker':is_jobseeker})
    
def resumes(request, resumes=None):
    if request.GET.get('bycat'):
        resumes = Resume.objects.filter(state='A').order_by('category')
    if resumes == None:
        resumes = Resume.objects.filter(state='A').order_by('id')
    paginator = Paginator(resumes, 6) # Show 6 contacts per page
    page = request.GET.get('page')
    try:
        resumes = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        resumes = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        resumes = paginator.page(paginator.num_pages)
    # pages = ads.count()/6
    return render(request, "resumes.html",{'resumes' : resumes,})

@login_required
def resume(request):
    id = request.GET.get('id')
    resume = Resume.objects.get(id=id)
    is_employer = False
    if Employer.objects.filter(user = request.user):
        is_employer = True
    return render(request, "resume.html",{'resume' : resume, 'is_employer': is_employer})

def festivals(request):
    festivals = Festival.objects.all().order_by('id')
    paginator = Paginator(festivals, 6) # Show 6 contacts per page
    page = request.GET.get('page')
    try:
        ads = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        ads = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        festivals = paginator.page(paginator.num_pages)

    return render(request, "festivals.html",{'fes' : festivals})

def faq(request):
    faqs = FAQ.objects.all().order_by('id')
    return render(request, "faq.html",{'aqs' : faqs,})

##def evaluation(request):
##   # id = request.GET.get('id')
##    if request.user.is_authenticated():#login bashe
##        comments = Comment.objects.filter(user = request.user)#cmnts khode user bashe ke nist
##    if comments:
##        return render(request, "evaluation.html",{'cmts' : comments,})
##    return render(request, "evaluation.html")

@login_required
def evaluation(request):
    us1 = User.objects.filter(user=request.user)
    us = us1[0]
    flag = JobSeeker.objects.filter(user=request.user).exists()
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            # process the form!
            print("im valid you know")
            comment = form.save(commit=False)
            print(request.user)
            comment.user= us
            comment.save()

            request.session["kind"] = comment.kind
            request.session["user"] = comment.user
        else:
            return render(request, "evaluation.html",{'form':form, 'flag':flag})
    else:
        KIND_CHOICES = request.session.get('kind', '')
        user = request.session.get('user', '')
        form = CommentForm(
            initial={'kind': KIND_CHOICES, 'user': user}
        )
    comments = Comment.objects.filter(user=us)

    return render(request, "evaluation.html", locals())

@login_required
@employer_required
def advertisement_register(request):
    if request.account.verified_by_human != 'A':
        message = _("Your account has not been verified by admin. You can not post any advertisement.")
        return render(request, "advertisement_register.html", {'message': message, 'flag': False})
    if request.method == "POST":
        form = AdvertisementRegisterForm(request.POST, request.FILES)
        if form.is_valid():
            ad = form.save(commit=False)
            ad.employer = request.account
            ad.save()
            return HttpResponseRedirect("/")
    else:
        form = AdvertisementRegisterForm()
    return render(request, "advertisement_register.html", {'form': form, 'flag': False})

@login_required
@employer_required
def special_ad_register(request, id):
   print ("cruel")
   ad = Advertisement.objects.get(pk = id)
   payment = Payment(amount = 1000)
   
   if request.method == "POST":
       form = SpecialAdvertisementRegisterForm(request.POST, request.FILES)
       
       if form.is_valid():
           myAdv = form.save(commit=False)
           start_date = form.cleaned_data['from_date']
           end_date = form.cleaned_data['to_date']
           amount = (end_date - start_date).days
           print (amount)
           amount = amount * 1000
           payment.amount = amount
           payment.save()
           myAdv.advertisement = ad
           myAdv.payment = payment
           print("hey buddy")
           myAdv.save()
           print('yo')
           return special_ad_register1(request, id, amount)
   else:
       form = SpecialAdvertisementRegisterForm()
   return render(request, "special_ad_register.html", {'form': form, 'pay':payment, 'adi': ad})

def special_ad_register1(request, id, amount):
    message = u"مبلغ "
    message += str(amount)
    message += u"تومان "
    message += u"با موفقيت پرداخت شد"
    return render(request, 'alert.html',{'message':message,})             

    
@login_required
@jobseeker_required
def add_resume(request):
    print("1")
    if request.method == "POST":
        form = ResumeRegisterForm(request.POST)
        if form.is_valid():
            reume = form.save(commit=False)
            resume.job_seeker = request.account
            resume.save()
            print("1")
            return HttpResponseRedirect("/")
    else:
        print("1")
        form = ResumeRegisterForm()
    return render(request, "advertisement_register.html", {'form': form, 'flag': True})
    
@login_required
@jobseeker_required
def update_resume(request, id):
    seeker = JobSeeker.objects.get(user = request.user)
    resume = Resume.objects.get(pk = id)
    if resume.job_seeker != seeker:
        return HttpResponseRedirect("/account/")
        
    if request.method == "POST":
        form = ResumeRegisterForm(request.POST, instance = resume)
        print("request.post ", request.POST)
        if form.is_valid():
            resume = form.save()
            return HttpResponseRedirect("/account/")
    
    else:
        form = ResumeRegisterForm(instance=resume)
    return render(request, 'update.html', {'form': form, 'flag' : True, 'res': resume})

@login_required
@employer_required  
def update_ad(request, id):
    employer = Employer.objects.get(user = request.user)
    ad = Advertisement.objects.get(pk = id)
    if ad.employer != employer:
        return HttpResponseRedirect("/account/")
        
    if request.method == "POST":
        form = AdvertisementRegisterForm(request.POST, instance = ad)
        print("request.post ", request.POST)
        if form.is_valid():
            ad = form.save()
            return HttpResponseRedirect("/account/")
    
    else:
        form = AdvertisementRegisterForm(instance=ad)
    return render(request, 'update.html', {'form': form, 'flag' : False, 'res': ad})
    


@login_required
def change_password(request):
    flag = JobSeeker.objects.filter(user=request.user).exists()
    return password_change(request, template_name='change_password.html', post_change_redirect='/account/',\
extra_context={'flag': flag})
def contact_us(request):
    flag = JobSeeker.objects.filter(user=request.user).exists()

    return render(request, "contact_us.html", {'flag':flag})


def reset_password(request):
    encoded = request.GET.get('q')
    if encoded:
        email = base64.b64decode(encoded)
        email=email.decode("utf-8")
        print(email)
        try:
            account = User.objects.get(email=email)
        except User.DoesNotExist:
            message2=u"ایمیل وارد شده در این درگاه نامعتبر است"
            return render(request, 'alert.html',{'message':message2,'type':'warning'})

    if request.method == "POST":
        form = ChangePassForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data['password']
            account.user.set_password(password)
            account.user.save()
            message2=u"پسورد شما با موفقیت تغییر یافت"
            return render(request, 'alert.html',{'message':message2,'type':'success'})
    else:
        form = ChangePassForm()
    return render(request, 'newpassword.html',{'form':form,'email':encoded})



def forgot_password(request):
    if request.method == "POST":
        form = ForgetPassForm(request.POST)
        if form.is_valid():
            email=form.cleaned_data['email']
            return forgot_password1(email,request)
    else:
        form = ForgetPassForm()
    return render(request, "forgot_password.html", {'form': form})

def forgot_password1(email,request):
    message = u".لینک درخواست گذرواژه جدید به ایمیل شما ارسال شد"
    recipients=[]
    recipients.append(email)
    messag = "localhost:8000/resetpass/?q="
    r = email.encode('utf-8')
    encoded = base64.b64encode(r)
    messag+=encoded.decode("utf-8")
    subject="Change password link"
    sender="info@goods.com"
    messag = mark_safe(render_to_string("alert.html"))
    send_mail(subject, messag, sender, recipients)
    return render(request, 'alert.html',{'message':message,'type':'success'})


@employer_required
def request_resume(request):

    if request.method == "POST":
        #from hidden input get id
        data = request.POST
        advertisement = Advertisement.objects.get(id = data['ad'])
        resume = Resume.objects.get(id= data['resume'])
        jobSeeker = Resume.objects.get(id= data['resume']).job_seeker
        jobrequest = JobRequest(advertisement = advertisement,job_seeker=jobSeeker,direction='E')
        jobrequest.save()
        email = jobSeeker.email
        subject="Request for your resume"
        sender="info@goods.com"
        recipients=[]
        recipients.append(email)
        message = "کارفرما با مشخصات زیر برای رزومه شما درخواست ارسال کرده است:\n"
        
        
        html_content = render_to_string('email.html', {'ad':advertisement}) # ...
        text_content = strip_tags(html_content) # this strips the html, so people will have the text as well.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(subject, text_content, sender, recipients)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        
        
        # message += advertisement.employer.__unicode__()
        send_mail(subject, message, sender, recipients)
        message = "درخواست شما برای "
        message+= jobSeeker.name.encode('utf-8')
        message+=" ارسال گردید . "
        return render(request, 'alert.html',{'message':message,'type': 'success'})
        
    resume = Resume.objects.get(id= request.GET.get('id'))
    employer = Employer.objects.get(user = request.user)
    ads = Advertisement.objects.filter(employer=employer)
    return render(request, 'select_ad.html',{'ads':ads , 'resume':resume})

@jobseeker_required
def ad_request(request):
    advertisement = Advertisement.objects.get(id = request.GET.get('id'))
    jobSeeker = JobSeeker.objects.get(user = request.user)
    jobrequest = JobRequest(advertisement = advertisement,job_seeker=jobSeeker,direction='S')
    jobrequest.save()
    email = advertisement.employer.email
    print(email)
    subject="Request for your ad"
    sender="info@goods.com"
    recipients=[]
    recipients.append(email)
    message = "درخواست شما برای "
    message+= advertisement.employer.name.encode('utf-8')
    message+=" ارسال گردید . "
    html_content = render_to_string('emailtoEmployer.html', {'job_seeker':jobSeeker,'resume': jobSeeker.resume_set.all()[0]}) # ...
    text_content = strip_tags(html_content) # this strips the html, so people will have the text as well.
    # create the email, and attach the HTML version as well.
    msg = EmailMultiAlternatives(subject, text_content, sender, recipients)
    msg.attach_alternative(html_content, "text/html")
    msg.send()
    
    return render(request, 'alert.html',{'message':message,'type': 'success'})
    
def remove_ad(request, id):
    ad = Advertisement.objects.get(pk = id)
    employer = Employer.objects.filter(user = request.user)
    employer = employer[0]
    if ad.employer != employer:
        return render(request, 'alert.html',{'message':"شما اجازه ی این کار را ندارید",'type': 'warning'})
    ad.state = 'D'
    ad.save()
    return HttpResponseRedirect("/account/")
    
def remove_res(request, id):
    res = Resume.objects.get(pk = id)
    j = JobSeeker.objects.filter(user = request.user)
    j = j[0]
    if res.job_seeker != j:
        return render(request, 'alert.html',{'message':"شما اجازه ی این کار را ندارید",'type': 'warning'})
    res.state = 'D'
    res.save()
    return HttpResponseRedirect("/account/")
def accept_request(request, id):
    jobrequest = JobRequest.objects.get(pk = id)
    if jobrequest.job_seeker.user != request.user or jobrequest.direction != 'E':
        return render(request, 'alert.html',{'message':"شما اجازه ی این کار را ندارید",'type': 'warning'})
    jobrequest.success = 'T'
    jobrequest.save()
    return HttpResponseRedirect("/jobproposal/")

def reject_request(request, id):
    jobrequest = JobRequest.objects.get(pk = id)
    if jobrequest.job_seeker.user != request.user or jobrequest.direction != 'E':
        return render(request, 'alert.html',{'message':"شما اجازه ی این کار را ندارید",'type': 'warning'})
    jobrequest.success = 'F'
    jobrequest.save()
    return HttpResponseRedirect("/jobproposal/")
def emp_reject_request(request, id):
    jobrequest = JobRequest.objects.get(pk = id)
    if jobrequest.advertisement.employer.user != request.user or jobrequest.direction != 'S':
        return render(request, 'alert.html',{'message':"شما اجازه ی این کار را ندارید",'type': 'warning'})
    jobrequest.success = 'F'
    jobrequest.save()
    return HttpResponseRedirect("/empJobproposal/")

    
def emp_accept_request(request, id):
    jobrequest = JobRequest.objects.get(pk = id)
    if jobrequest.advertisement.employer.user != request.user or jobrequest.direction != 'S':
        return render(request, 'alert.html',{'message':"شما اجازه ی این کار را ندارید",'type': 'warning'})
    jobrequest.success = 'T'
    jobrequest.save()
    return HttpResponseRedirect("/empJobproposal/")

    
    
        

    
    
        
    

