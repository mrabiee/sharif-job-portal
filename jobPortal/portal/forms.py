from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User as djangoUser
from portal.models import JobSeeker, Employer, Festival, SpecialAdvertisement, Advertisement,Resume,Comment
    
class SeekerRegisterForm(forms.ModelForm):
    password = forms.CharField(label=_("Password"), max_length=30, widget=forms.PasswordInput)
    confirm_password = forms.CharField(label=_("Confirm Password"), max_length=30, widget=forms.PasswordInput)

    class Meta:
        model = JobSeeker
        fields = ('email', 'name', 'telephone', 'mobilephone',
                    'address', 'postal_code', 'national_code', 'gender', 'date_of_birth')
        widgets = {
                'password': forms.PasswordInput(),
        }

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial', {})
        initial['date_of_birth'] = 'yyyy-mm-dd'
        kwargs['initial'] = initial
        super(SeekerRegisterForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        email = self.cleaned_data.get('email', None)
        if email and djangoUser.objects.filter(username=email):
            raise forms.ValidationError(_("This user exists"))
        return email

    def clean(self):
        cleaned_data = super(SeekerRegisterForm, self).clean()
        password = cleaned_data.get('password', None)
        confirm = cleaned_data.get('confirm_password', None)
        if not password or not confirm or password != confirm:
            raise forms.ValidationError(_("Password and Confirm Password should be the same!"))
        return cleaned_data

    def save(self):
        acc = super(SeekerRegisterForm, self).save(commit=False)
        user = djangoUser.objects.create_user(self.cleaned_data['email'], self.cleaned_data['email'], self.cleaned_data['password'])
        acc.user = user
        acc.save()
        return acc
        
class SeekerUpdate(forms.ModelForm):
    class Meta:
        model = JobSeeker
        fields = ('email', 'name', 'telephone', 'mobilephone',
                    'address', 'postal_code', 'national_code', 'gender', 'date_of_birth')

    def __init__(self, user, *args, **kwargs):
        self.user = user
        initial = kwargs.get('initial', {})
        initial['date_of_birth'] = 'yyyy-mm-dd'
        kwargs['initial'] = initial
        super(SeekerUpdate, self).__init__(*args, **kwargs)
        
    def clean_email(self):
        email = self.cleaned_data.get('email', None)
        seeker = JobSeeker.objects.get(user = self.user)
        if email and djangoUser.objects.filter(username=email).exclude(username = seeker.email):
            raise forms.ValidationError(_("This user exists"))
        else:
            return email

class EmployerUpdate(forms.ModelForm):
    class Meta:
        model = Employer
        fields = ('email', 'name', 'telephone', 'mobilephone',
        'address', 'postal_code', 'economic_code', 'registration_number')
    
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(EmployerUpdate, self).__init__(*args, **kwargs)
    
    
    
    
    def clean_email(self):
        email = self.cleaned_data.get('email', None)
        employer = Employer.objects.get(user = self.user)
        if email and djangoUser.objects.filter(username=email).exclude(username = employer.email):
            raise forms.ValidationError(_("This user exists"))
        else:
            return email
        
    
class EmployerRegisterForm(forms.ModelForm):
    password = forms.CharField(label=_("Password"), max_length=30, widget=forms.PasswordInput)
    confirm_password = forms.CharField(label=_("Confirm Password"), max_length=30, widget=forms.PasswordInput)

    class Meta:
        model = Employer
        fields = ('email', 'name', 'telephone', 'mobilephone',
                    'address', 'postal_code', 'economic_code', 'registration_number')
        widgets = {
                'password': forms.PasswordInput(),
        }

    def clean_email(self):
        email = self.cleaned_data.get('email', None)
        if email and djangoUser.objects.filter(username=email):
            raise forms.ValidationError(_("This user exists"))
        return email

    def clean(self):
        cleaned_data = super(EmployerRegisterForm, self).clean()
        password = cleaned_data.get('password', None)
        confirm = cleaned_data.get('confirm_password', None)
        if not password or not confirm or password != confirm:
            raise forms.ValidationError(_("Password and Confirm Password should be the same!"))
        return cleaned_data

    def save(self):
        acc = super(EmployerRegisterForm, self).save(commit=False)
        user = djangoUser.objects.create_user(self.cleaned_data['email'], self.cleaned_data['email'], self.cleaned_data['password'])
        acc.user = user
        acc.save()
        return acc

class AdvertisementRegisterForm(forms.ModelForm):
    class Meta:
        model = Advertisement
        fields = ('city', 'salary', 'gender', 'age', 'number_of_people',
                    'state', 'required_skills', 'work_hours', 'addres',
                    'category', 'logo', 'education')

class SpecialAdvertisementRegisterForm(forms.ModelForm):

##    def __init__(self, *args, **kwargs):
##        super(SpecialAdvertisementRegisterForm, self).__init__(*args, **kwargs)
##        self.fields['from_date'].widget.input_type = 'date'
##        self.fields['to_date'].widget = widgets.AdminDateWidget()
        
    class Meta:
        model = SpecialAdvertisement
        fields = ('from_date', 'to_date')


    
class ResumeRegisterForm(forms.ModelForm):
    class Meta:
        model = Resume
        fields = ('job_experiences', 'education', 'skills', 'category')

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ['kind', 'text']


    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs) # Call to ModelForm constructor
        self.fields['kind'].widget.attrs['class'] = 'radio'
        self.fields['text'].widget.attrs['class'] = 'form-control'
        self.fields['text'].widget.attrs['row'] = '3'
        
class ForgetPassForm(forms.Form):
    email = forms.EmailField(label=_("Email"))
    
class ChangePassForm(forms.Form):
    password = forms.CharField(label=_("Password"), max_length=30, widget=forms.PasswordInput)
    confirm_password = forms.CharField(label=_("Confirm Password"), max_length=30, widget=forms.PasswordInput)

    class Meta:
        widgets = {
                'password': forms.PasswordInput(),
        }

    def clean(self):
        cleaned_data = super(ChangePassForm, self).clean()
        password = cleaned_data.get('password', None)
        confirm = cleaned_data.get('confirm_password', None)
        if not password or not confirm or password != confirm:
            raise forms.ValidationError(_("Password and Confirm Password should be the same!"))
        return cleaned_data
