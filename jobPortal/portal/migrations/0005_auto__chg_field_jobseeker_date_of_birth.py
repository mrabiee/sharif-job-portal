# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'JobSeeker.date_of_birth'
        db.alter_column(u'portal_jobseeker', 'date_of_birth', self.gf('django.db.models.fields.DateField')())

    def backwards(self, orm):

        # Changing field 'JobSeeker.date_of_birth'
        db.alter_column(u'portal_jobseeker', 'date_of_birth', self.gf('django.db.models.fields.DateTimeField')())

    models = {
        u'portal.advertisement': {
            'Meta': {'object_name': 'Advertisement'},
            'addres': ('django.db.models.fields.TextField', [], {}),
            'age': ('django.db.models.fields.IntegerField', [], {'default': '18'}),
            'category': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '2'}),
            'city': ('django.db.models.fields.CharField', [], {'default': "'T'", 'max_length': '1'}),
            'employer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Employer']", 'null': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number_of_people': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'required_skills': ('django.db.models.fields.TextField', [], {}),
            'salary': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'}),
            'work_hours': ('django.db.models.fields.IntegerField', [], {'default': '8'})
        },
        u'portal.comment': {
            'Meta': {'object_name': 'Comment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kind': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.User']"})
        },
        u'portal.employer': {
            'Meta': {'object_name': 'Employer', '_ormbases': [u'portal.User']},
            'economic_code': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'registration_number': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.User']", 'unique': 'True', 'primary_key': 'True'}),
            'verified_by_human': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '1'})
        },
        u'portal.festival': {
            'Meta': {'object_name': 'Festival'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'from_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'portal.jobrequest': {
            'Meta': {'object_name': 'JobRequest'},
            'advertisement': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Advertisement']"}),
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_seeker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.JobSeeker']"}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'portal.jobseeker': {
            'Meta': {'object_name': 'JobSeeker', '_ormbases': [u'portal.User']},
            'date_of_birth': ('django.db.models.fields.DateField', [], {}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'national_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'portal.payment': {
            'Meta': {'object_name': 'Payment'},
            'amount': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'refrence_code': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'portal.registerfestival': {
            'Meta': {'object_name': 'RegisterFestival'},
            'employer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Employer']"}),
            'festival': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Festival']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Payment']"})
        },
        u'portal.resume': {
            'Meta': {'object_name': 'Resume'},
            'category': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '2'}),
            'education': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_experiences': ('django.db.models.fields.TextField', [], {}),
            'job_seeker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.JobSeeker']"}),
            'skills': ('django.db.models.fields.TextField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'})
        },
        u'portal.specialadvertisement': {
            'Meta': {'object_name': 'SpecialAdvertisement', '_ormbases': [u'portal.Advertisement']},
            u'advertisement_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.Advertisement']", 'unique': 'True', 'primary_key': 'True'}),
            'from_date': ('django.db.models.fields.DateTimeField', [], {}),
            'payment': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.Payment']", 'unique': 'True', 'null': 'True'}),
            'to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'portal.user': {
            'Meta': {'object_name': 'User'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobilephone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'password': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'blank': 'True'}),
            'payment': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.Payment']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['portal']