# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'User.username'
        db.delete_column(u'portal_user', 'username')

        # Deleting field 'User.password'
        db.delete_column(u'portal_user', 'password')

        # Adding field 'User.user'
        db.add_column(u'portal_user', 'user',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['auth.User']),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'User.username'
        raise RuntimeError("Cannot reverse this migration. 'User.username' and its values cannot be restored.")
        # Adding field 'User.password'
        db.add_column(u'portal_user', 'password',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Deleting field 'User.user'
        db.delete_column(u'portal_user', 'user_id')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'portal.advertisement': {
            'Meta': {'object_name': 'Advertisement'},
            'addres': ('django.db.models.fields.TextField', [], {}),
            'age': ('django.db.models.fields.IntegerField', [], {'default': '18'}),
            'category': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '2'}),
            'city': ('django.db.models.fields.CharField', [], {'default': "'T'", 'max_length': '1'}),
            'employer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Employer']", 'null': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number_of_people': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'required_skills': ('django.db.models.fields.TextField', [], {}),
            'salary': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'}),
            'work_hours': ('django.db.models.fields.IntegerField', [], {'default': '8'})
        },
        u'portal.comment': {
            'Meta': {'object_name': 'Comment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kind': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.User']"})
        },
        u'portal.employer': {
            'Meta': {'object_name': 'Employer', '_ormbases': [u'portal.User']},
            'economic_code': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'registration_number': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.User']", 'unique': 'True', 'primary_key': 'True'}),
            'verified_by_human': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '1'})
        },
        u'portal.festival': {
            'Meta': {'object_name': 'Festival'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'from_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poster': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'portal.jobrequest': {
            'Meta': {'object_name': 'JobRequest'},
            'advertisement': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Advertisement']"}),
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_seeker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.JobSeeker']"}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'portal.jobseeker': {
            'Meta': {'object_name': 'JobSeeker', '_ormbases': [u'portal.User']},
            'date_of_birth': ('django.db.models.fields.DateField', [], {}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'national_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'portal.payment': {
            'Meta': {'object_name': 'Payment'},
            'amount': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'refrence_code': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'portal.registerfestival': {
            'Meta': {'object_name': 'RegisterFestival'},
            'employer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Employer']"}),
            'festival': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Festival']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Payment']"})
        },
        u'portal.resume': {
            'Meta': {'object_name': 'Resume'},
            'category': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '2'}),
            'education': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_experiences': ('django.db.models.fields.TextField', [], {}),
            'job_seeker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.JobSeeker']"}),
            'skills': ('django.db.models.fields.TextField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'})
        },
        u'portal.specialadvertisement': {
            'Meta': {'object_name': 'SpecialAdvertisement', '_ormbases': [u'portal.Advertisement']},
            u'advertisement_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.Advertisement']", 'unique': 'True', 'primary_key': 'True'}),
            'from_date': ('django.db.models.fields.DateTimeField', [], {}),
            'payment': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.Payment']", 'unique': 'True', 'null': 'True'}),
            'to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'portal.user': {
            'Meta': {'object_name': 'User'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobilephone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'payment': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.Payment']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        }
    }

    complete_apps = ['portal']