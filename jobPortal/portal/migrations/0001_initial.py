# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'portal_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('telephone', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('mobilephone', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('address', self.gf('django.db.models.fields.TextField')()),
            ('postal_code', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('password', self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True)),
            ('payment', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['portal.Payment'], unique=True, null=True)),
        ))
        db.send_create_signal(u'portal', ['User'])

        # Adding model 'JobSeeker'
        db.create_table(u'portal_jobseeker', (
            (u'user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['portal.User'], unique=True, primary_key=True)),
            ('national_code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('date_of_birth', self.gf('django.db.models.fields.DateTimeField')()),
            ('gender', self.gf('django.db.models.fields.CharField')(default='M', max_length=1)),
        ))
        db.send_create_signal(u'portal', ['JobSeeker'])

        # Adding model 'Employer'
        db.create_table(u'portal_employer', (
            (u'user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['portal.User'], unique=True, primary_key=True)),
            ('economic_code', self.gf('django.db.models.fields.CharField')(max_length=12)),
            ('registration_number', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('verified_by_human', self.gf('django.db.models.fields.CharField')(default='N', max_length=1)),
        ))
        db.send_create_signal(u'portal', ['Employer'])

        # Adding model 'Resume'
        db.create_table(u'portal_resume', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('job_experiences', self.gf('django.db.models.fields.TextField')()),
            ('education', self.gf('django.db.models.fields.TextField')()),
            ('skills', self.gf('django.db.models.fields.TextField')()),
            ('state', self.gf('django.db.models.fields.CharField')(default='A', max_length=1)),
            ('job_seeker', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal.JobSeeker'])),
            ('category', self.gf('django.db.models.fields.CharField')(default='S', max_length=1)),
        ))
        db.send_create_signal(u'portal', ['Resume'])

        # Adding model 'Advertisement'
        db.create_table(u'portal_advertisement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('city', self.gf('django.db.models.fields.CharField')(default='T', max_length=1)),
            ('salary', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('gender', self.gf('django.db.models.fields.CharField')(default='M', max_length=1)),
            ('age', self.gf('django.db.models.fields.IntegerField')(default=18)),
            ('number_of_people', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('state', self.gf('django.db.models.fields.CharField')(default='A', max_length=1)),
            ('required_skills', self.gf('django.db.models.fields.TextField')()),
            ('work_hours', self.gf('django.db.models.fields.IntegerField')(default=8)),
            ('addres', self.gf('django.db.models.fields.TextField')()),
            ('category', self.gf('django.db.models.fields.CharField')(default='S', max_length=1)),
        ))
        db.send_create_signal(u'portal', ['Advertisement'])

        # Adding model 'SpecialAdvertisement'
        db.create_table(u'portal_specialadvertisement', (
            (u'advertisement_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['portal.Advertisement'], unique=True, primary_key=True)),
            ('from_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('to_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('payment', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['portal.Payment'], unique=True, null=True)),
        ))
        db.send_create_signal(u'portal', ['SpecialAdvertisement'])

        # Adding model 'Festival'
        db.create_table(u'portal_festival', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('from_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('to_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'portal', ['Festival'])

        # Adding model 'Payment'
        db.create_table(u'portal_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('amount', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('refrence_code', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'portal', ['Payment'])

        # Adding model 'RegisterFestival'
        db.create_table(u'portal_registerfestival', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('employer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal.Employer'])),
            ('festival', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal.Festival'])),
            ('payment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal.Payment'])),
        ))
        db.send_create_signal(u'portal', ['RegisterFestival'])

        # Adding model 'JobRequest'
        db.create_table(u'portal_jobrequest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('advertisement', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal.Advertisement'])),
            ('job_seeker', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal.JobSeeker'])),
            ('direction', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal(u'portal', ['JobRequest'])

        # Adding model 'Comment'
        db.create_table(u'portal_comment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('kind', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal.User'])),
        ))
        db.send_create_signal(u'portal', ['Comment'])


    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'portal_user')

        # Deleting model 'JobSeeker'
        db.delete_table(u'portal_jobseeker')

        # Deleting model 'Employer'
        db.delete_table(u'portal_employer')

        # Deleting model 'Resume'
        db.delete_table(u'portal_resume')

        # Deleting model 'Advertisement'
        db.delete_table(u'portal_advertisement')

        # Deleting model 'SpecialAdvertisement'
        db.delete_table(u'portal_specialadvertisement')

        # Deleting model 'Festival'
        db.delete_table(u'portal_festival')

        # Deleting model 'Payment'
        db.delete_table(u'portal_payment')

        # Deleting model 'RegisterFestival'
        db.delete_table(u'portal_registerfestival')

        # Deleting model 'JobRequest'
        db.delete_table(u'portal_jobrequest')

        # Deleting model 'Comment'
        db.delete_table(u'portal_comment')


    models = {
        u'portal.advertisement': {
            'Meta': {'object_name': 'Advertisement'},
            'addres': ('django.db.models.fields.TextField', [], {}),
            'age': ('django.db.models.fields.IntegerField', [], {'default': '18'}),
            'category': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1'}),
            'city': ('django.db.models.fields.CharField', [], {'default': "'T'", 'max_length': '1'}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number_of_people': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'required_skills': ('django.db.models.fields.TextField', [], {}),
            'salary': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'}),
            'work_hours': ('django.db.models.fields.IntegerField', [], {'default': '8'})
        },
        u'portal.comment': {
            'Meta': {'object_name': 'Comment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kind': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.User']"})
        },
        u'portal.employer': {
            'Meta': {'object_name': 'Employer', '_ormbases': [u'portal.User']},
            'economic_code': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'registration_number': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.User']", 'unique': 'True', 'primary_key': 'True'}),
            'verified_by_human': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '1'})
        },
        u'portal.festival': {
            'Meta': {'object_name': 'Festival'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'from_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'portal.jobrequest': {
            'Meta': {'object_name': 'JobRequest'},
            'advertisement': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Advertisement']"}),
            'direction': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_seeker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.JobSeeker']"})
        },
        u'portal.jobseeker': {
            'Meta': {'object_name': 'JobSeeker', '_ormbases': [u'portal.User']},
            'date_of_birth': ('django.db.models.fields.DateTimeField', [], {}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'national_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'portal.payment': {
            'Meta': {'object_name': 'Payment'},
            'amount': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'refrence_code': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'portal.registerfestival': {
            'Meta': {'object_name': 'RegisterFestival'},
            'employer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Employer']"}),
            'festival': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Festival']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.Payment']"})
        },
        u'portal.resume': {
            'Meta': {'object_name': 'Resume'},
            'category': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1'}),
            'education': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_experiences': ('django.db.models.fields.TextField', [], {}),
            'job_seeker': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['portal.JobSeeker']"}),
            'skills': ('django.db.models.fields.TextField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1'})
        },
        u'portal.specialadvertisement': {
            'Meta': {'object_name': 'SpecialAdvertisement', '_ormbases': [u'portal.Advertisement']},
            u'advertisement_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.Advertisement']", 'unique': 'True', 'primary_key': 'True'}),
            'from_date': ('django.db.models.fields.DateTimeField', [], {}),
            'payment': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.Payment']", 'unique': 'True', 'null': 'True'}),
            'to_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'portal.user': {
            'Meta': {'object_name': 'User'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobilephone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'password': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'blank': 'True'}),
            'payment': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['portal.Payment']", 'unique': 'True', 'null': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['portal']