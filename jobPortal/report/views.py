import datetime
from json import dumps

from django.http import HttpResponse
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render
from django.db.models import Sum, Count

from portal.models import Payment, JobSeeker, Employer, Comment, JobRequest

@staff_member_required
def payments(request):
    dates = []
    sales = []
    now = datetime.date.today()
    for i in range(14,0,-1):
        time = now - datetime.timedelta(days=i)
        dates.append(time.strftime('%d %b'))
        total = Payment.objects.filter(specialadvertisement__from_date=time).aggregate(Sum('amount'))
        total = total.get('amount__sum', 0) or 0
        sales.append(total/1000)
    payments = Payment.objects.select_related('specialadvertisement')
    return render(request, "payments.html", {'dates': dates, 'sales': sales, 'payments': payments})

@staff_member_required
def seekers(request):
    dates = []
    registers = []
    now = datetime.date.today()
    for i in range(14,0,-1):
        time = now - datetime.timedelta(days=i)
        dates.append(time.strftime('%d %b'))
        total = JobSeeker.objects.filter(register_date=time).aggregate(Count('id'))
        total = total.get('id__count', 0) or 0
        registers.append(total)
    seeker = JobSeeker.objects.order_by('register_date')[:100]
    return render(request, "seekers.html", {'dates': dates, 'registers': registers, 'seekers': seeker})

@staff_member_required
def employers(request):
    dates = []
    registers = []
    now = datetime.date.today()
    for i in range(14,0,-1):
        time = now - datetime.timedelta(days=i)
        dates.append(time.strftime('%d %b'))
        total = Employer.objects.filter(register_date=time).aggregate(Count('id'))
        total = total.get('id__count', 0) or 0
        registers.append(total)
    employer = Employer.objects.order_by('register_date')[:100]
    return render(request, "employers.html", {'dates': dates, 'registers': registers, 'employers': employer})

@staff_member_required
def comments(request):
    comment = Comment.objects.order_by('register_date')[:100]
    return render(request, "comments.html", {'comments': comment})

@staff_member_required
def success(request):
    requests = JobRequest.objects.filter(success='T').order_by('id')[:100]
    return render(request, "success.html", {'requests': requests})

@staff_member_required
def verify(request):
    employers = Employer.objects.filter(verified_by_human='N')
    return render(request, "verify.html", {'employers': employers})

@staff_member_required
def ajax(request):
    id_e = request.GET.get('id', None)
    state = request.GET.get('state', None)
    emp = Employer.objects.get(id=id_e)
    if state == 'V':
        emp.verified_by_human = 'A'
    else:
        emp.verified_by_human = 'D'
    emp.save()
    return HttpResponse(dumps({'error':0}),content_type ='application/json')
