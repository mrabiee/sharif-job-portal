from django.conf.urls import patterns, include, url

urlpatterns = patterns('report.views',
    url(r'^$', 'payments'),
    url(r'^seekers/$', 'seekers'),
    url(r'^employers/$', 'employers'),
    url(r'^comments/$', 'comments'),
    url(r'^success/$', 'success'),
    url(r'^verify/$', 'verify'),
    url(r'^ajax/$', 'ajax'),
)